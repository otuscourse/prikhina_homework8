﻿namespace ConsoleApp1
{
    public class FileWork
    {
        public static void CreateFiles()
        {
            File.WriteAllText(@"C:\folder\file1.txt", "Прочитать 3 файла параллельно и вычислить количество пробелов в них (через Task).");
            File.WriteAllText(@"C:\folder\file2.txt", "Написать функцию, принимающую в качестве аргумента путь к папке. Из этой папки параллельно прочитать все файлы и вычислить количество пробелов в них.");
            File.WriteAllText(@"C:\folder\file3.txt", "Замерьте время выполнения кода (класс Stopwatch).");
        }

        public static string[] GetFiles(string dirPath)
        {
            return Directory.GetFiles(dirPath,"*.txt");
        }
    }
}
