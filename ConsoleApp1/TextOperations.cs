﻿namespace ConsoleApp1
{
    public class TextOperations
    {
        public static void SpaceCount(string fileName, bool writeLine)
        {
            var str = File.ReadAllText(fileName);
            var spaceCount = str.ToArray().Where(ch => ch == ' ').ToArray().Length;
            if (writeLine)
                Console.WriteLine($"Файл: {Path.GetFileName(fileName)}\nСтрока: {str}\nКоличество пробелов: {spaceCount}\n");
        }
    }
}
