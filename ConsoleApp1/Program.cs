﻿using System.Diagnostics;

namespace ConsoleApp1
{
    public class Program
    {
        static void Main(string[] args)
        {
            TimeWithTasks();
            TimeWithoutTasks();
            Console.ReadLine();
        }

        public static void TimeWithTasks()
        {
            FileWork.CreateFiles();

            var stopWatch = new Stopwatch();
            stopWatch.Start();
            Console.WriteLine("Прочитать 3 файла параллельно и вычислить количество пробелов в них:\n");

            var task1 = Task.Run(() => TextOperations.SpaceCount(@"C:\folder\file1.txt", true));
            var task2 = Task.Run(() => TextOperations.SpaceCount(@"C:\folder\file2.txt", true));
            var task3 = Task.Run(() => TextOperations.SpaceCount(@"C:\folder\file3.txt", true));

            Task.WaitAll(task1, task2, task3);

            Console.WriteLine("\nПрочитать файлы из папки параллельно и вычислить количество пробелов в них:\n");
            var files = FileWork.GetFiles(@"C:\folder");
            var tasks = new Task[files.Length];
            int num = 0;
            foreach(var file in files)
            {
                tasks[num] = Task.Run(() => TextOperations.SpaceCount(file, true));
                num++;
            }

            Task.WaitAll(tasks);
            stopWatch.Stop();
            Console.WriteLine($"\nВремя выполнения в параллельных потоках: {stopWatch.ElapsedMilliseconds}мс");
        }

        public static void TimeWithoutTasks()
        {
            FileWork.CreateFiles();

            var stopWatch = new Stopwatch();
            stopWatch.Start();

            TextOperations.SpaceCount(@"C:\folder\file1.txt", false);
            TextOperations.SpaceCount(@"C:\folder\file2.txt", false);
            TextOperations.SpaceCount(@"C:\folder\file3.txt", false);

            var files = FileWork.GetFiles(@"C:\folder");
            foreach (var file in files)
            {
                TextOperations.SpaceCount(file, false);
            }
            stopWatch.Stop();
            Console.WriteLine($"\nВремя выполнения в без параллельных потоков: {stopWatch.ElapsedMilliseconds}мс");
        }
    }

}
